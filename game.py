from random import randint
# randint returns a random number between/including the two arguments passed in

# Task
# player_name = prompt the player for their name
player_name = str(input("Hi! What is your name?"))


# Guess 1
# month_number = generate a random number
month_number = randint(1, 12)

# year_number = generate a random number
year_number = randint(1924, 2004)

print(
    f"Guess 1 :, {player_name}, were you born in {month_number} / {year_number}?")

# response = prompt the player with "yes or no?"
response = input("Yes or no")
# if response is "yes", then
if response == "yes":
    # print the message "I knew it!"
    print("I knew it!")
    exit()
# otherwise,
# print the message "Drat! Lemme try again!"
else:
    print("Drat! Lemme try again!")

# Guess 2
month_number = randint(1, 12)
year_number = randint(1924, 2004)
print(
    f"Guess 2 :, {player_name}, were you born in {month_number} / {year_number}?")
response = input("Yes or no")
if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Guess 3
month_number = randint(1, 12)
year_number = randint(1924, 2004)
print(
    f"Guess 3 :, {player_name}, were you born in {month_number} / {year_number}?")
response = input("Yes or no")
if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Guess 4
month_number = randint(1, 12)
year_number = randint(1924, 2004)
print(
    f"Guess 4 :, {player_name}, were you born in {month_number} / {year_number}?")
response = input("Yes or no")
if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Last Guess
month_number = randint(1, 12)
year_number = randint(1924, 2004)
print(
    f"Guess 5 :, {player_name}, were you born in {month_number} / {year_number}?")
response = input("Yes or no")
if response == "yes":
    print("I knew it!")
else:
    print("I have other things to do. Good bye.")
